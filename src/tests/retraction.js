const BinomoAPI = require('../services/BinomoAPI');

// 120 62 { total: 44, winRate: 0.5909090909090909, gain: 2.8000000000000007 }
// 80 62 { total: 43, winRate: 0.6046511627906976, gain: 3.8000000000000007 }
// 
// 
// 
// 
// 

(async () => {
    const MIN_CANDLES = 60;
    const history = []
    const api = new BinomoAPI()
    const allCandlesNoTime = await api.getCandles('AUD/USD', 30, 10)
    const allCandles = allCandlesNoTime.map(candle => { return { ...candle, time: new Date(candle.created_at) } })
    allCandles.forEach((candle, index) => {
        if (index < MIN_CANDLES - 2) return;
        if (!allCandles[index + 3]) return;

        const initialIndex = index - MIN_CANDLES + 2;
        const candles = allCandles.slice(initialIndex, initialIndex + MIN_CANDLES);
        const currentCandle = allCandles[index + 2];
        const nextCandle = allCandles[index + 3];
        const previousCandle = candles.slice(-1)[0];

        if (currentCandle.time.getSeconds() != 0) return;
        if (currentCandle.time.getHours() <= 4) return
        if (currentCandle.time.getHours() >= 18) return

        let avgRatio = 0;

        let candlesCount = 0

        candles.forEach(candle => {
            const bodyCandleSize = Math.abs(candle.close - candle.open);
            const tailCandleSize = (candle.high - candle.low) - bodyCandleSize;
            const tailRatio = tailCandleSize / (bodyCandleSize + tailCandleSize) || 0;
            if (tailRatio != 0) {
                candlesCount++
                avgRatio += tailRatio;
            }
        })
        avgRatio /= candlesCount;
        if (avgRatio < .62) return;
        // console.log('opa', avgRatio)

        const bodyCandleSize = Math.abs(previousCandle.close - previousCandle.open);
        const tailCandleSize = (previousCandle.high - previousCandle.low) - bodyCandleSize;
        const tailRatio = tailCandleSize / (bodyCandleSize + tailCandleSize) || 0
        const candleSize = previousCandle.high - previousCandle.low;
        let highValue = previousCandle.high + candleSize * .0;
        let lowValue = previousCandle.low - candleSize * .0;

        if (tailRatio < .3) return


        if (currentCandle.high >= highValue) {
            if (nextCandle.close < highValue) {
                history.push({ time: currentCandle.time, result: 'won' });
            }
            if (nextCandle.close > highValue) {
                history.push({ time: currentCandle.time, result: 'lost' });
            }
        }
        if (currentCandle.low <= lowValue) {
            if (nextCandle.close > lowValue) history.push({ time: currentCandle.time, result: 'won' });
            if (nextCandle.close < lowValue) history.push({ time: currentCandle.time, result: 'lost' });
        }
    })

    const wons = history.filter(result => result.result == 'won').length
    const losses = history.filter(result => result.result == 'lost').length
    const result = { total: history.length, winRate: wons / (wons + losses), gain: (wons * .80 - losses * 1) };
    console.log(result)
})();

