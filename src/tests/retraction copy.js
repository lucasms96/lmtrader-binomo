const BinomoAPI = require('../services/BinomoAPI');

(async () => {
    const MIN_CANDLES = 12;
    const history = []
    const api = new BinomoAPI()
    const allCandlesNoTime = await api.getCandles('AUD/USD', 60, 10)
    const allCandles = allCandlesNoTime.map(candle => { return { ...candle, time: new Date(candle.created_at) } })
    allCandles.forEach((candle, index) => {
        if (index < MIN_CANDLES - 2) return;
        if (!allCandles[index + 3]) return;

        const initialIndex = index - MIN_CANDLES + 2;
        const candles = allCandles.slice(initialIndex, initialIndex + MIN_CANDLES);
        const currentCandle = allCandles[index + 2];
        const nextCandle = allCandles[index + 3];
        const previousCandle = candles.slice(-1)[0];

        // if (currentCandle.time.getSeconds() != 0) return;

        let avgRatio = 0;

        let candlesCount = 0

        candles.forEach(candle => {
            const bodyCandleSize = Math.abs(candle.close - candle.open);
            const tailCandleSize = (candle.high - candle.low) - bodyCandleSize;
            const tailRatio = tailCandleSize / (bodyCandleSize + tailCandleSize) || 0;
            if (tailRatio != 0) {
                candlesCount++
                avgRatio += tailRatio;
            }
        })
        avgRatio /= candlesCount;
        if (avgRatio < .7) return;
        // console.log('opa', avgRatio)
 
        let highValue = previousCandle.high
        let lowValue = previousCandle.low

        const bodyCandleSize = Math.abs(previousCandle.close - previousCandle.open);
        const tailCandleSize = (previousCandle.high - previousCandle.low) - bodyCandleSize;
        const tailRatio = tailCandleSize / (bodyCandleSize + tailCandleSize) || 0

        if (tailRatio < .1) return

        if (currentCandle.high >= highValue) {
            if (nextCandle.close < highValue) {
                history.push({ time: currentCandle.time.toLocaleString(), result: 'won' });
            }
            if (nextCandle.close > highValue) {
                history.push({ time: currentCandle.time.toLocaleString(), result: 'lost' });
            }
        }
        if (currentCandle.low <= lowValue) {
            if (nextCandle.close > lowValue)
                history.push({ time: currentCandle.time.toLocaleString(), result: 'won' });
            if (nextCandle.close < lowValue)
                history.push({ time: currentCandle.time.toLocaleString(), result: 'lost' });
        }
    })

    const wons = history.filter(result => result.result == 'won').length
    const losses = history.filter(result => result.result == 'lost').length
    const result = { total: history.length, winRate: wons / (wons + losses), gain: (wons * .80 - losses * 1) };
    // console.log(history)
    console.log(result)
})();

