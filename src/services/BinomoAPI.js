const axios = require('axios');

class BinomoAPI {
    constructor() {
        this.api = axios.create({
            baseURL: 'https://api.binomo.com'
        });
        this.api.defaults.headers.common['device-type'] = 'web';
        this.api.defaults.headers.common['user-agent'] = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.71 Safari/537.36';
        this.api.defaults.headers.common['content-type'] = 'application/json';
        this.api.defaults.headers.common['device-id'] = '2b5d84d7fb019ff4e2dae86b425aa615';
        this.api.defaults.headers.common['authorization-token'] = '07c38351-4f83-4a43-83e3-54cf671857f9';
        this.api.defaults.headers.common['sec-ch-ua-platform'] = '"Windows"';
    }
    async getProfile() {
        const result = await this.api({
            method: 'get',
            url: 'private/v2/profile'
        });
    }
    async getAssets() {
        const result = await this.api({ method: 'get', url: 'private/v2/assets' });
        return result.data.data.assets;
    }
    async getCandles(assetName, seconds = 60, times = 1) {
        const periods = [{
            period: 1,
            frame: 1,
            interval: 15,
            timeUnit: "minute"
        }, {
            period: 5,
            frame: 5,
            interval: 1,
            timeUnit: "hour"
        }, {
            period: 15,
            frame: 15,
            interval: 4,
            timeUnit: "hour"
        }, {
            period: 30,
            frame: 30,
            interval: 12,
            timeUnit: "hour"
        }, {
            period: 60,
            frame: 60,
            interval: 24,
            timeUnit: "hour"
        }, {
            period: 300,
            frame: 300,
            interval: 4,
            timeUnit: "day"
        }, {
            period: 900,
            frame: 900,
            interval: 12,
            timeUnit: "day"
        }, {
            period: 1800,
            frame: 1800,
            interval: 24,
            timeUnit: "day"
        }, {
            period: 3600,
            frame: 3600,
            interval: 48,
            timeUnit: "day"
        }, {
            period: 10800,
            frame: 10800,
            interval: 144,
            timeUnit: "day"
        }, {
            period: 86400,
            frame: 86400,
            interval: 1152,
            timeUnit: "day"
        }, {
            period: 2592e3,
            frame: 2592e3,
            interval: 34560,
            timeUnit: "day"
        }];

        const period = periods.find(period => period.period == seconds)
        const currentDate = new Date();
        const e = period.interval
        if (period.timeUnit == 'minute') {
            const t = currentDate.getTime()
            const n = 60000
            currentDate.setTime(Math.floor(t / (e * n)) * (e * n))
        }

        if (period.timeUnit == 'hour') {
            const t = currentDate.getTime()
            const n = 3600000
            currentDate.setTime(Math.floor(t / (e * n)) * (e * n))
        }
        if (period.timeUnit == 'day') {
            const t = currentDate.getTime()
            const n = 3600000 * 24
            currentDate.setTime(Math.floor(t / (e * n)) * (e * n))
        }

        let candles = []
        for (let time = 0; time < times; time++) {
            // const url = '/candles/v1/EURO/2022-01-16T00:00:00/3600?locale=br'
            const url = `candles/v1/${assetName}/${currentDate.toISOString().split(".")[0]}/${seconds}?locale=br`
            const newCandles = (await this.api({ method: 'get', url: url })).data.data;
            console.log(`candles/v1/${assetName}/${currentDate.toISOString().split(".")[0]}/${seconds}?locale=br`)
            candles = newCandles.concat(candles)

            if (period.timeUnit == 'minute') {
                const t = currentDate.getTime()
                const n = 60000
                currentDate.setTime(Math.floor(t / (e * n)) * (e * n) - (e * n))
            }
    
            if (period.timeUnit == 'hour') {
                const t = currentDate.getTime()
                const n = 3600000
                currentDate.setTime(Math.floor(t / (e * n)) * (e * n) - (e * n))
            }
            if (period.timeUnit == 'day') {
                const t = currentDate.getTime()
                const n = 3600000 * 24
                currentDate.setTime(Math.floor(t / (e * n)) * (e * n) - (e * n))
            }

        }
        return candles
    }//https://api.binomo.com/candles/v1/EURO/2022-01-16T00:00:00/3600?locale=br

}

module.exports = BinomoAPI;