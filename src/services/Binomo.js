const WebSocket = require('websocket').w3cwebsocket;
const BinomoAPI = require('./BinomoAPI');

class Binomo {
    constructor() {
        this.binomoAPI = new BinomoAPI();
        this.profile = null;
        this.assets = {};
        this.ws = null;
    }
    async start() {
        await this.initWS();
        console.log('Broadcast iniciado');
        this.profile = await this.binomoAPI.getProfile();
        this.updateAssets();
        this.updateSubscribe();
        this.listenCandles();
    }
    async initWS() {
        return new Promise((resolve, reject) => {
            this.ws = new WebSocket('wss://as.binomo.com/', [], 'https://binomo.com', {
                'Sec-WebSocket-Key': 'bkaQG2GgelPEsguCyhYeSQ==',
                'Origin': 'https://binomo.com',
                'Host': 'as.binomo.com'
            });
            this.ws.onopen = (msg) => {
                resolve()
            };
            this.ws.onclose = (msg) => console.log(msg);
            this.ws.onerror = (msg) => console.log(msg);
            this.ws.onmessage = (msg) => console.log(msg);
        })
    }
    updateSubscribe() {
        const assets = [];
        for (const assetRic in this.assets) {
            assets.push(assetRic);
        }
        this.ws.send(JSON.stringify({ "action": "subscribe", "rics": assets }));
    }
    listenCandles() {
        const onMessage = (data) => {
            console.log(data)
        }
        this.ws.addEventListener('message', onMessage)
    }

    async updateAssets() {
        const assets = await this.binomoAPI.getAssets();
        assets.forEach(asset => {
            this.assets[asset.ric] = { ...asset[asset.ric], ...asset };
        });
        for (const assetRic in this.assets) {
            const asset = this.assets[assetRic];
            if (!this.filterAsset(asset)) {
                delete this.assets[assetRic];
                continue;
            }
            if (!asset.candles) this.updateCandles(asset);
        }
    }
    subscribe() {

    }
    updateCandles(asset) {
        this.binomoAPI.getCandles(asset.ric).then(candles => {
            asset.candles = candles
        });
    }
    filterAsset(asset) {
        if (!asset.active) return;
        if (asset.base_payment_rate_turbo < 80) return;
        return true;
    }
}


module.exports = Binomo;