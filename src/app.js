const WebSocket = require('ws');

var ws = new WebSocket('wss://ws.binomo.com/?authtoken=a61db450-9499-4dce-b469-3221d3e56098&device=web&device_id=dc2e72b7950cf1d2b033e3df00b21799&v=2&vsn=2.0.0', {
    origin: 'https://binomo.com',
    headers: {
        'Host': 'ws.binomo.com',
        'Connection': 'Upgrade',
        'Pragma': 'no-cache',
        'Cache-Control': 'no-cache',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36',
        'Upgrade': 'websocket',
        'Origin': 'https://binomo.com'
    }
});

ws.onerror = function() {
    console.log('Connection Error');
};

ws.onopen = function() {
    console.log('WebSocket Client Connected');

    ws.send(JSON.stringify({"topic":"account","event":"phx_join","payload":{},"ref":"5","join_ref":"5"}))
    ws.send(JSON.stringify({"topic":"user","event":"phx_join","payload":{},"ref":"8","join_ref":"8"}))
    ws.send(JSON.stringify({"topic":"bo","event":"phx_join","payload":{},"ref":"11","join_ref":"11"}))
    ws.send(JSON.stringify({"topic":"tournament","event":"phx_join","payload":{},"ref":"14","join_ref":"14"})) 
    
};

ws.onclose = function(msg) {
    console.log(msg)
    console.log('echo-protocol Client Closed');
};

ws.onmessage = function(e) {
    if (typeof e.data === 'string') {
        console.log("Received: '" + e.data + "'");
    }
};
// const ws = new WebSocket({
//     server: 'wss://ws.binomo.com/?authtoken=a61db450-9499-4dce-b469-3221d3e56098&device=web&device_id=dc2e72b7950cf1d2b033e3df00b21799&v=2&vsn=2.0.0',
//     verifyClient: function (info, done) {
//         console.log(info)
//         process.exit()
//         // let query = url.parse(info.req.url, true).query;
//         // jwt.verify(query.token, config.jwt.secret, function (err, decoded) {
//         //     if (err) return done(false, 403, 'Not valid token');

//         //     // Saving the decoded JWT on the client would be nice
//         //     done(true);
//         // });
//     }
// });

// ws.on('connection', function connection(ws, req) {
//     console.log('wtf')
//     const ip = req.headers['x-forwarded-for'].split(',')[0].trim();
// });




// ws.on('headers', function open() {
//     console.log('headers')
// });



/*
var W3CWebSocket = require('websocket').w3cwebsocket;


var client = new W3CWebSocket('wss://ws.binomo.com/?authtoken=a61db450-9499-4dce-b469-3221d3e56098&device=web&device_id=dc2e72b7950cf1d2b033e3df00b21799&v=2&vsn=2.0.0');

*/